﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(business_on.Startup))]
namespace business_on
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
